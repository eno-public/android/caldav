package at.bitfire.davdroid.model

import androidx.room.PrimaryKey

abstract class IdEntity {
    abstract var id: Long
}