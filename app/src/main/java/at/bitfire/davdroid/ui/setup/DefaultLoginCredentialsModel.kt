package at.bitfire.davdroid.ui.setup

import android.content.Intent
import androidx.annotation.MainThread
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DefaultLoginCredentialsModel : ViewModel() {

    private var initialized = false

    val loginWithEmailAddress = MutableLiveData<Boolean>()
    val loginWithUrlAndUsername = MutableLiveData<Boolean>()
    val loginWithUrlAndCertificate = MutableLiveData<Boolean>()

    var loginInvisible = MutableLiveData<Boolean>()

    val baseUrl = MutableLiveData<String>()
    val baseUrlError = MutableLiveData<String>()

    /** user name or email address */
    val username = MutableLiveData<String>()
    val usernameError = MutableLiveData<String>()

    val password = MutableLiveData<String>()
    val passwordError = MutableLiveData<String>()

    val certificateAlias = MutableLiveData<String>()
    val certificateAliasError = MutableLiveData<String>()

    init {
        loginWithEmailAddress.value = true
    }

    @MainThread
    fun initialize(intent: Intent) {
        if (initialized)
            return
        initialized = true

        // we've got initial login data
        var givenUrl = intent.getStringExtra(LoginActivity.EXTRA_URL)
        var givenUsername = intent.getStringExtra(LoginActivity.EXTRA_USERNAME)
        var givenPassword = intent.getStringExtra(LoginActivity.EXTRA_PASSWORD)

        //Sandeep Emekar: Added deeplinking
        var data = intent?.data
        givenUrl = data?.getQueryParameter("url")
        givenUsername = data?.getQueryParameter("username")
        givenPassword = data?.getQueryParameter("password")
        //END

        if (givenUrl != null) {
            loginWithUrlAndUsername.value = true
            baseUrl.value = givenUrl
        } else {
            loginWithEmailAddress.value = true
            username.value = givenUsername
        }
        password.value = givenPassword
    }

}